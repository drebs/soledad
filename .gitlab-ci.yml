---
stages:
  - code-check
  - build
  - tests
  - package
  - benchmarks


variables:
  DOCKER_DRIVER: overlay

# Cache a folder between builds.
# Tox sets it to be our pip cache.
cache:
  untracked: true
  key: soledad-shared-pip-cache
  paths:
    - .cache/

before_script:
  - echo "Running on ${HOST_HOSTNAME:=$(hostname)}"


#
# "code-check" stage
#

code-check:
  stage: code-check
  except:
    - schedules
  image: 0xacab.org:4567/leap/soledad:latest
  script:
    - tox -e code-check


#
# "build" stage
#

build_docker_image:
  stage: build
  except:
    - schedules
  image: 0xacab.org:4567/leap/soledad:latest
  services:
    - docker:dind
  tags:
    - docker-in-docker
  script:
    - scripts/docker/build-docker-image.sh


#
# "tests" stage
#

tests:
  stage: tests
  except:
    - schedules
  image: 0xacab.org:4567/leap/soledad:latest
  services:
    - couchdb
  script:
    - tox -- --couch-url http://couchdb:5984

#e2e:
#  stage: tests
#  except:
#    - schedules
#  image: 0xacab.org:4567/leap/soledad:latest
#  script:
#    - tox -e e2e


#
# "package" stage
#

.job_template: &package
  stage: package
  except:
    - schedules
  image: "0xacab.org:4567/leap/gitlab-buildpackage:build_${DIST}_${ARCH}"
  script:
    - /usr/bin/unbuffer scripts/packaging/run-packaging-ci-job.sh | /usr/bin/ts -s
    # sleep 1h to allow debugging of running container
    # - sleep 3600
  artifacts:
    expire_in: 1w
    paths:
      - '*_*.xz'
      - '*_*.dsc'
      - '*_amd64.changes'
      - '*.deb'
      - 'results/*'
  cache:
    policy: pull


package:amd64_stretch:
  variables:
    ARCH: "amd64"
    DIST: "stretch"
    REPONAMES: "platform,client"
    # Default is to fail on warnings, we disable it here
    # unless a manpage is included (see #8895)
    LINTIAN_OPTS: "--fail-on-warnings -X filename-length,manpages,standards-version"
  <<: *package

package:amd64_buster:
  variables:
    ARCH: "amd64"
    DIST: "buster"
    REPONAMES: "client"
    # Default is to fail on warnings, we disable it here
    # unless a manpage is included (see #8895)
    # Also, ignore the `build-depends-on-obsolete-package` tag
    # until we can deprecate jessie and take out the dh-systemd
    # build-depends (see #8963)
    LINTIAN_OPTS: "--fail-on-warnings -X filename-length,manpages --suppress-tags build-depends-on-obsolete-package"
  <<: *package

package:amd64_sid:
  variables:
    ARCH: "amd64"
    DIST: "sid"
    REPONAMES: "client"
    # Default is to fail on warnings, we disable it here
    # unless a manpage is included (see #8895)
    LINTIAN_OPTS: "--fail-on-warnings -X filename-length,manpages --suppress-tags build-depends-on-obsolete-package"
  <<: *package

package:amd64_artful:
  variables:
    ARCH: "amd64"
    DIST: "artful"
    REPONAMES: "client"
    # Default is to fail on warnings, we disable it here
    # unless a manpage is included (see #8895)
    LINTIAN_OPTS: "--fail-on-warnings -X filename-length,manpages --suppress-tags build-depends-on-obsolete-package"
  <<: *package

package:amd64_bionic:
  variables:
    ARCH: "amd64"
    DIST: "bionic"
    REPONAMES: "client"
    # Default is to fail on warnings, we disable it here
    # unless a manpage is included (see #8895)
    LINTIAN_OPTS: "--fail-on-warnings -X filename-length,manpages --suppress-tags build-depends-on-obsolete-package"
  <<: *package

#
# "benchmarks" stage
#

.job_template: &benchmark
  stage: benchmarks
  only:
    - schedules
  image: 0xacab.org:4567/leap/soledad:latest
  tags:
    - benchmark
  services:
    - couchdb
  before_script:
    - git checkout -B "$CI_COMMIT_REF_NAME" "$CI_COMMIT_SHA"
    - '[ -n "${BENCHMARK_ALL_COMMITS}" ] && git checkout origin/master scripts/benchmark/setup-all-commits-env.sh'
    - '[ -n "${BENCHMARK_ALL_COMMITS}" ] && ./scripts/benchmark/setup-all-commits-env.sh'
    - curl -s couchdb:5984
    # You can provide a $NETRC variable containing the creds for your
    # elasticsearch instance so it's protected from being leaked in the
    # CI console
    # We can't get it working inside docker for unknown reasons.
    # - echo "$NETRC" > /root/.netrc && chmod 600 /root/.netrc
    #
    # Add $PYTEST_OPTS to pytest.ini to allow posting benchmark tests
    # to an elasticsearch instance
    - echo "addopts=$PYTEST_OPTS" >> pytest.ini && chmod 600 pytest.ini
  after_script:
    # Output locally saved benchmarks if they exist
    - 'if [ -d .benchmarks ]; then find .benchmarks -type f -exec cat {} \; ; fi'

time-cpu:
  <<: *benchmark
  script:
    - scripts/benchmark/run-benchmarks-ci-job.sh benchmark-time-cpu

memory:
  <<: *benchmark
  script:
    - scripts/benchmark/run-benchmarks-ci-job.sh benchmark-memory

responsiveness:
  <<: *benchmark
  script:
    - scripts/benchmark/run-benchmarks-ci-job.sh responsiveness
